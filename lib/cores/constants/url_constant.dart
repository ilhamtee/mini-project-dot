class URLConstant {
  URLConstant._();

  static const String baseURL = 'https://dot-mobile-test.web.app';
  static const String getPlaces = '/place.json';
  static const String getGalleries = '/gallery.json';
  static const String getUserProfile = '/user.json';
}
