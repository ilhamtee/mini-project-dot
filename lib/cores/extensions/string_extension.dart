extension StringExtension on String {
  bool get isValidEmail {
    final RegExp emailRegex =
        RegExp(r'^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$');

    return emailRegex.hasMatch(this);
  }

  bool get isValidPassword {
    if (length < 6) {
      return false;
    }

    if (!contains(RegExp(r'[A-Z]'))) {
      return false;
    }

    if (!contains(RegExp(r'[a-z]'))) {
      return false;
    }

    return true;
  }
}
