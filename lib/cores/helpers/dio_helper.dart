import 'dart:developer';

import 'package:dio/dio.dart';
import 'package:injectable/injectable.dart';

@injectable
class DioHelper {
  static Dio? dio;

  static void initialDio(String baseUrl) {
    dio = Dio(
      BaseOptions(
        baseUrl: baseUrl,
        connectTimeout: 20000,
        receiveTimeout: 40000,
        contentType: 'application/json',
        responseType: ResponseType.json,
      ),
    );
  }

  static String generalException(DioError e) {
    String message = 'Failed to process your request';

    if (e.response != null) {
      log('DioError response status: ${e.response!.statusCode}');
      log('DioError response data: ${e.response!.data}');
      log('DioError response headers: ${e.response!.headers}');
      if (e.response?.data != null) {
        message = e.response?.data['message'] ?? 'Something went wrong';
      }
    } else {
      log('DioError: ${e.message}');
      message = e.message;
    }
    return message;
  }
}
