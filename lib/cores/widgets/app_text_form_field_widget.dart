import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

typedef OnTextFieldValidate = String? Function(String?);

class AppTextFormField extends StatelessWidget {
  const AppTextFormField({
    super.key,
    required this.controller,
    required this.hintText,
    this.focusNode,
    this.hintStyle,
    this.inputBorder,
    this.contentPadding,
    this.isPassword = false,
    this.suffixIcon,
    this.keyboardType,
    this.inputFormatters,
    this.onChanged,
    this.prefixIcon,
    this.autoValidateMode,
    this.validator,
  });

  final TextEditingController controller;
  final FocusNode? focusNode;
  final TextStyle? hintStyle;
  final String hintText;
  final InputBorder? inputBorder;
  final EdgeInsets? contentPadding;
  final bool isPassword;
  final Widget? suffixIcon;
  final Widget? prefixIcon;
  final TextInputType? keyboardType;
  final List<TextInputFormatter>? inputFormatters;
  final ValueChanged<String>? onChanged;
  final AutovalidateMode? autoValidateMode;
  final OnTextFieldValidate? validator;

  @override
  Widget build(BuildContext context) {
    return TextFormField(
      controller: controller,
      focusNode: focusNode,
      onChanged: onChanged,
      obscureText: isPassword,
      inputFormatters: inputFormatters,
      keyboardType: keyboardType,
      validator: validator,
      autovalidateMode: autoValidateMode,
      decoration: InputDecoration(
        hintText: hintText,
        hintStyle: hintStyle,
        contentPadding: contentPadding ??
            const EdgeInsets.symmetric(vertical: 10, horizontal: 12),
        prefixIcon: prefixIcon,
        prefixIconColor: Colors.green.shade300,
        enabledBorder: inputBorder ??
            const OutlineInputBorder(
              borderSide: BorderSide(width: 1, color: Colors.green),
            ),
        focusedBorder: inputBorder ??
            const OutlineInputBorder(
              borderSide: BorderSide(width: 2, color: Colors.green),
            ),
        errorBorder: const OutlineInputBorder(
          borderSide: BorderSide(width: 2, color: Colors.red),
        ),
        focusedErrorBorder: const OutlineInputBorder(
          borderSide: BorderSide(width: 2, color: Colors.red),
        ),
      ),
    );
  }
}
