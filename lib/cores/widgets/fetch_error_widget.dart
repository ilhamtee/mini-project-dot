import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';

class FetchErrorWidget extends StatelessWidget {
  const FetchErrorWidget(
      {super.key, required this.errorMessage, this.onTapReload});

  final String errorMessage;
  final VoidCallback? onTapReload;

  @override
  Widget build(BuildContext context) {
    return RichText(
      text: TextSpan(
          text: errorMessage,
          style: const TextStyle(color: Colors.black),
          children: [
            TextSpan(
                text: ' Reload Page',
                recognizer: TapGestureRecognizer()..onTap = onTapReload,
                style: const TextStyle(
                    color: Colors.blue, fontWeight: FontWeight.w600))
          ]),
      textAlign: TextAlign.center,
    );
  }
}
