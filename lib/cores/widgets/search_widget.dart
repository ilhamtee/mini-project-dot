import 'package:flutter/material.dart';
import 'package:mini_project/cores/widgets/app_text_form_field_widget.dart';

class SearchWidget extends StatelessWidget {
  const SearchWidget(
      {super.key,
      required this.searchController,
      required this.searchFocus,
      required this.onChanged});

  final TextEditingController searchController;
  final FocusNode searchFocus;
  final ValueChanged<String> onChanged;

  @override
  Widget build(BuildContext context) {
    return AppTextFormField(
      controller: searchController,
      focusNode: searchFocus,
      onChanged: onChanged,
      hintText: 'Search ...',
      prefixIcon: const Icon(Icons.search),
    );
  }
}
