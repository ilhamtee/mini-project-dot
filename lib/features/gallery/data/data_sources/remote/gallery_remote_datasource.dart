import 'package:dartz/dartz.dart';
import 'package:mini_project/cores/exception/exception.dart';
import 'package:mini_project/cores/use_case/use_case.dart';
import 'package:mini_project/features/gallery/data/models/gallery_model.dart';

abstract class GalleryRemoteDataSource {
  Future<Either<Failures, List<GalleryModel>>> getGalleries(
    NoParams noParams,
  );
}
