import 'package:dartz/dartz.dart';
import 'package:dio/dio.dart';
import 'package:injectable/injectable.dart';
import 'package:mini_project/cores/constants/url_constant.dart';
import 'package:mini_project/cores/exception/exception.dart';
import 'package:mini_project/cores/extensions/dio_response_extension.dart';
import 'package:mini_project/cores/helpers/dio_helper.dart';
import 'package:mini_project/cores/use_case/use_case.dart';
import 'package:mini_project/features/gallery/data/data_sources/remote/gallery_remote_datasource.dart';
import 'package:mini_project/features/gallery/data/models/gallery_model.dart';

@LazySingleton(as: GalleryRemoteDataSource)
class GalleryRemoteDataSourceImpl implements GalleryRemoteDataSource {
  final Dio _dio;

  GalleryRemoteDataSourceImpl(this._dio);

  @override
  Future<Either<Failures, List<GalleryModel>>> getGalleries(
      NoParams noParams) async {
    try {
      final response = await _dio.get(
        URLConstant.getGalleries,
      );

      if (response.isOk) {
        final List<dynamic> rawGalleriesData = response.data['data'];

        final List<GalleryModel> mappedGalleriesData =
            rawGalleriesData.map((e) => GalleryModel.fromJson(e)).toList();

        return Right(mappedGalleriesData);
      }

      return const Right([]);
    } on DioError catch (e) {
      final message = DioHelper.generalException(e);
      return Left(ServerFailure(errorMessage: message));
    }
  }
}
