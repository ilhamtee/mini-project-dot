import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:mini_project/features/gallery/domain/entities/gallery_entity.dart';

part 'gallery_model.freezed.dart';
part 'gallery_model.g.dart';

@freezed
class GalleryModel with _$GalleryModel {
  factory GalleryModel({
    required String caption,
    required String thumbnail,
    required String image,
  }) = _GalleryModel;

  factory GalleryModel.fromJson(Map<String, dynamic> json) =>
      _$GalleryModelFromJson(json);
}

extension GalleryModelX on GalleryModel {
  GalleryEntity toDomain() =>
      GalleryEntity(caption: caption, thumbnail: thumbnail, image: image);
}
