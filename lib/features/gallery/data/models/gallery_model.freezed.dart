// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target

part of 'gallery_model.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

GalleryModel _$GalleryModelFromJson(Map<String, dynamic> json) {
  return _GalleryModel.fromJson(json);
}

/// @nodoc
mixin _$GalleryModel {
  String get caption => throw _privateConstructorUsedError;
  String get thumbnail => throw _privateConstructorUsedError;
  String get image => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $GalleryModelCopyWith<GalleryModel> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $GalleryModelCopyWith<$Res> {
  factory $GalleryModelCopyWith(
          GalleryModel value, $Res Function(GalleryModel) then) =
      _$GalleryModelCopyWithImpl<$Res, GalleryModel>;
  @useResult
  $Res call({String caption, String thumbnail, String image});
}

/// @nodoc
class _$GalleryModelCopyWithImpl<$Res, $Val extends GalleryModel>
    implements $GalleryModelCopyWith<$Res> {
  _$GalleryModelCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? caption = null,
    Object? thumbnail = null,
    Object? image = null,
  }) {
    return _then(_value.copyWith(
      caption: null == caption
          ? _value.caption
          : caption // ignore: cast_nullable_to_non_nullable
              as String,
      thumbnail: null == thumbnail
          ? _value.thumbnail
          : thumbnail // ignore: cast_nullable_to_non_nullable
              as String,
      image: null == image
          ? _value.image
          : image // ignore: cast_nullable_to_non_nullable
              as String,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$_GalleryModelCopyWith<$Res>
    implements $GalleryModelCopyWith<$Res> {
  factory _$$_GalleryModelCopyWith(
          _$_GalleryModel value, $Res Function(_$_GalleryModel) then) =
      __$$_GalleryModelCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call({String caption, String thumbnail, String image});
}

/// @nodoc
class __$$_GalleryModelCopyWithImpl<$Res>
    extends _$GalleryModelCopyWithImpl<$Res, _$_GalleryModel>
    implements _$$_GalleryModelCopyWith<$Res> {
  __$$_GalleryModelCopyWithImpl(
      _$_GalleryModel _value, $Res Function(_$_GalleryModel) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? caption = null,
    Object? thumbnail = null,
    Object? image = null,
  }) {
    return _then(_$_GalleryModel(
      caption: null == caption
          ? _value.caption
          : caption // ignore: cast_nullable_to_non_nullable
              as String,
      thumbnail: null == thumbnail
          ? _value.thumbnail
          : thumbnail // ignore: cast_nullable_to_non_nullable
              as String,
      image: null == image
          ? _value.image
          : image // ignore: cast_nullable_to_non_nullable
              as String,
    ));
  }
}

/// @nodoc
@JsonSerializable()
class _$_GalleryModel implements _GalleryModel {
  _$_GalleryModel(
      {required this.caption, required this.thumbnail, required this.image});

  factory _$_GalleryModel.fromJson(Map<String, dynamic> json) =>
      _$$_GalleryModelFromJson(json);

  @override
  final String caption;
  @override
  final String thumbnail;
  @override
  final String image;

  @override
  String toString() {
    return 'GalleryModel(caption: $caption, thumbnail: $thumbnail, image: $image)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_GalleryModel &&
            (identical(other.caption, caption) || other.caption == caption) &&
            (identical(other.thumbnail, thumbnail) ||
                other.thumbnail == thumbnail) &&
            (identical(other.image, image) || other.image == image));
  }

  @JsonKey(ignore: true)
  @override
  int get hashCode => Object.hash(runtimeType, caption, thumbnail, image);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$_GalleryModelCopyWith<_$_GalleryModel> get copyWith =>
      __$$_GalleryModelCopyWithImpl<_$_GalleryModel>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$_GalleryModelToJson(
      this,
    );
  }
}

abstract class _GalleryModel implements GalleryModel {
  factory _GalleryModel(
      {required final String caption,
      required final String thumbnail,
      required final String image}) = _$_GalleryModel;

  factory _GalleryModel.fromJson(Map<String, dynamic> json) =
      _$_GalleryModel.fromJson;

  @override
  String get caption;
  @override
  String get thumbnail;
  @override
  String get image;
  @override
  @JsonKey(ignore: true)
  _$$_GalleryModelCopyWith<_$_GalleryModel> get copyWith =>
      throw _privateConstructorUsedError;
}
