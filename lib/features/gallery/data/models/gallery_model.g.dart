// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'gallery_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$_GalleryModel _$$_GalleryModelFromJson(Map<String, dynamic> json) =>
    _$_GalleryModel(
      caption: json['caption'] as String,
      thumbnail: json['thumbnail'] as String,
      image: json['image'] as String,
    );

Map<String, dynamic> _$$_GalleryModelToJson(_$_GalleryModel instance) =>
    <String, dynamic>{
      'caption': instance.caption,
      'thumbnail': instance.thumbnail,
      'image': instance.image,
    };
