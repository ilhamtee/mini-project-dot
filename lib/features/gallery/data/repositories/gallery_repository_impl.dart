import 'package:dartz/dartz.dart';
import 'package:injectable/injectable.dart';
import 'package:mini_project/cores/exception/exception.dart';
import 'package:mini_project/cores/use_case/use_case.dart';
import 'package:mini_project/features/gallery/data/data_sources/remote/gallery_remote_datasource.dart';
import 'package:mini_project/features/gallery/data/models/gallery_model.dart';
import 'package:mini_project/features/gallery/domain/repositories/gallery_repository.dart';

@LazySingleton(as: GalleryRepository)
class GalleryRepositoryImpl implements GalleryRepository {
  final GalleryRemoteDataSource _remoteDataSource;

  GalleryRepositoryImpl(this._remoteDataSource);
  @override
  Future<Either<Failures, List<GalleryModel>>> getGalleries(
      NoParams noParams) async {
    return await _remoteDataSource.getGalleries(noParams);
  }
}
