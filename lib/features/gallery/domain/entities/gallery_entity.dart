import 'package:equatable/equatable.dart';

class GalleryEntity extends Equatable {
  final String caption;
  final String thumbnail;
  final String image;

  const GalleryEntity(
      {required this.caption, required this.thumbnail, required this.image});

  @override
  List<Object?> get props => [caption, thumbnail, image];
}
