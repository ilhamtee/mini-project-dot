import 'package:dartz/dartz.dart';
import 'package:injectable/injectable.dart';
import 'package:mini_project/cores/exception/exception.dart';
import 'package:mini_project/cores/use_case/use_case.dart';
import 'package:mini_project/features/gallery/data/models/gallery_model.dart';
import 'package:mini_project/features/gallery/domain/repositories/gallery_repository.dart';

@injectable
class FetchGalleriesUseCase implements UseCase<List<GalleryModel>, NoParams> {
  const FetchGalleriesUseCase(this._repository);
  final GalleryRepository _repository;

  @override
  Future<Either<Failures, List<GalleryModel>>> call(
    NoParams params,
  ) async {
    return await _repository.getGalleries(params);
  }
}
