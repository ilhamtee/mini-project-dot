import 'package:bloc/bloc.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:injectable/injectable.dart';
import 'package:mini_project/cores/use_case/use_case.dart';
import 'package:mini_project/features/gallery/data/models/gallery_model.dart';
import 'package:mini_project/features/gallery/domain/entities/gallery_entity.dart';
import 'package:mini_project/features/gallery/domain/use_cases/fetch_galleries_use_case.dart';

part 'gallery_event.dart';
part 'gallery_state.dart';
part 'gallery_bloc.freezed.dart';

@injectable
class GalleryBloc extends Bloc<GalleryEvent, GalleryState> {
  GalleryBloc(this._fetchGalleriesUseCase) : super(const GalleryState()) {
    on<FetchGalleriesEvent>(_onFetchGalleries);
    on<SearchGalleriesEvent>(_onSearchGalleries);
  }

  final FetchGalleriesUseCase _fetchGalleriesUseCase;

  Future<void> _onFetchGalleries(
      FetchGalleriesEvent event, Emitter<GalleryState> emit) async {
    emit(state.copyWith(status: GalleryStatus.loading));

    final fetchGalleriesResult = await _fetchGalleriesUseCase(NoParams());

    fetchGalleriesResult.fold(
        (failed) => emit(state.copyWith(
            message: failed.errorMessage,
            status: GalleryStatus.fetchGalleriesError)),
        (galleriesModel) => emit(state.copyWith(
            status: GalleryStatus.fetchGalleriesSuccess,
            defaultGalleries: galleriesModel.map((e) => e.toDomain()).toList(),
            galleries: galleriesModel.map((e) => e.toDomain()).toList())));
  }

  Future<void> _onSearchGalleries(
      SearchGalleriesEvent event, Emitter<GalleryState> emit) async {
    emit(state.copyWith(status: GalleryStatus.loading));

    final List<GalleryEntity> defaultGalleries = state.defaultGalleries;

    if (event.searchQuery.isNotEmpty) {
      final List<GalleryEntity> filteredGalleries = defaultGalleries
          .where((gallery) => gallery.caption
              .toLowerCase()
              .contains(event.searchQuery.toLowerCase()))
          .toList();

      emit(state.copyWith(
          status: GalleryStatus.searchGalleriesSuccess,
          galleries: filteredGalleries));
    } else {
      emit(state.copyWith(
          status: GalleryStatus.searchGalleriesSuccess,
          galleries: defaultGalleries));
    }
  }
}
