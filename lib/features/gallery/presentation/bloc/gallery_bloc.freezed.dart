// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target

part of 'gallery_bloc.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

/// @nodoc
mixin _$GalleryEvent {
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() fetchGalleries,
    required TResult Function(String searchQuery) searchGalleries,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? fetchGalleries,
    TResult? Function(String searchQuery)? searchGalleries,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? fetchGalleries,
    TResult Function(String searchQuery)? searchGalleries,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(FetchGalleriesEvent value) fetchGalleries,
    required TResult Function(SearchGalleriesEvent value) searchGalleries,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(FetchGalleriesEvent value)? fetchGalleries,
    TResult? Function(SearchGalleriesEvent value)? searchGalleries,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(FetchGalleriesEvent value)? fetchGalleries,
    TResult Function(SearchGalleriesEvent value)? searchGalleries,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $GalleryEventCopyWith<$Res> {
  factory $GalleryEventCopyWith(
          GalleryEvent value, $Res Function(GalleryEvent) then) =
      _$GalleryEventCopyWithImpl<$Res, GalleryEvent>;
}

/// @nodoc
class _$GalleryEventCopyWithImpl<$Res, $Val extends GalleryEvent>
    implements $GalleryEventCopyWith<$Res> {
  _$GalleryEventCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;
}

/// @nodoc
abstract class _$$FetchGalleriesEventCopyWith<$Res> {
  factory _$$FetchGalleriesEventCopyWith(_$FetchGalleriesEvent value,
          $Res Function(_$FetchGalleriesEvent) then) =
      __$$FetchGalleriesEventCopyWithImpl<$Res>;
}

/// @nodoc
class __$$FetchGalleriesEventCopyWithImpl<$Res>
    extends _$GalleryEventCopyWithImpl<$Res, _$FetchGalleriesEvent>
    implements _$$FetchGalleriesEventCopyWith<$Res> {
  __$$FetchGalleriesEventCopyWithImpl(
      _$FetchGalleriesEvent _value, $Res Function(_$FetchGalleriesEvent) _then)
      : super(_value, _then);
}

/// @nodoc

class _$FetchGalleriesEvent implements FetchGalleriesEvent {
  const _$FetchGalleriesEvent();

  @override
  String toString() {
    return 'GalleryEvent.fetchGalleries()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$FetchGalleriesEvent);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() fetchGalleries,
    required TResult Function(String searchQuery) searchGalleries,
  }) {
    return fetchGalleries();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? fetchGalleries,
    TResult? Function(String searchQuery)? searchGalleries,
  }) {
    return fetchGalleries?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? fetchGalleries,
    TResult Function(String searchQuery)? searchGalleries,
    required TResult orElse(),
  }) {
    if (fetchGalleries != null) {
      return fetchGalleries();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(FetchGalleriesEvent value) fetchGalleries,
    required TResult Function(SearchGalleriesEvent value) searchGalleries,
  }) {
    return fetchGalleries(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(FetchGalleriesEvent value)? fetchGalleries,
    TResult? Function(SearchGalleriesEvent value)? searchGalleries,
  }) {
    return fetchGalleries?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(FetchGalleriesEvent value)? fetchGalleries,
    TResult Function(SearchGalleriesEvent value)? searchGalleries,
    required TResult orElse(),
  }) {
    if (fetchGalleries != null) {
      return fetchGalleries(this);
    }
    return orElse();
  }
}

abstract class FetchGalleriesEvent implements GalleryEvent {
  const factory FetchGalleriesEvent() = _$FetchGalleriesEvent;
}

/// @nodoc
abstract class _$$SearchGalleriesEventCopyWith<$Res> {
  factory _$$SearchGalleriesEventCopyWith(_$SearchGalleriesEvent value,
          $Res Function(_$SearchGalleriesEvent) then) =
      __$$SearchGalleriesEventCopyWithImpl<$Res>;
  @useResult
  $Res call({String searchQuery});
}

/// @nodoc
class __$$SearchGalleriesEventCopyWithImpl<$Res>
    extends _$GalleryEventCopyWithImpl<$Res, _$SearchGalleriesEvent>
    implements _$$SearchGalleriesEventCopyWith<$Res> {
  __$$SearchGalleriesEventCopyWithImpl(_$SearchGalleriesEvent _value,
      $Res Function(_$SearchGalleriesEvent) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? searchQuery = null,
  }) {
    return _then(_$SearchGalleriesEvent(
      searchQuery: null == searchQuery
          ? _value.searchQuery
          : searchQuery // ignore: cast_nullable_to_non_nullable
              as String,
    ));
  }
}

/// @nodoc

class _$SearchGalleriesEvent implements SearchGalleriesEvent {
  const _$SearchGalleriesEvent({required this.searchQuery});

  @override
  final String searchQuery;

  @override
  String toString() {
    return 'GalleryEvent.searchGalleries(searchQuery: $searchQuery)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$SearchGalleriesEvent &&
            (identical(other.searchQuery, searchQuery) ||
                other.searchQuery == searchQuery));
  }

  @override
  int get hashCode => Object.hash(runtimeType, searchQuery);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$SearchGalleriesEventCopyWith<_$SearchGalleriesEvent> get copyWith =>
      __$$SearchGalleriesEventCopyWithImpl<_$SearchGalleriesEvent>(
          this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() fetchGalleries,
    required TResult Function(String searchQuery) searchGalleries,
  }) {
    return searchGalleries(searchQuery);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? fetchGalleries,
    TResult? Function(String searchQuery)? searchGalleries,
  }) {
    return searchGalleries?.call(searchQuery);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? fetchGalleries,
    TResult Function(String searchQuery)? searchGalleries,
    required TResult orElse(),
  }) {
    if (searchGalleries != null) {
      return searchGalleries(searchQuery);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(FetchGalleriesEvent value) fetchGalleries,
    required TResult Function(SearchGalleriesEvent value) searchGalleries,
  }) {
    return searchGalleries(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(FetchGalleriesEvent value)? fetchGalleries,
    TResult? Function(SearchGalleriesEvent value)? searchGalleries,
  }) {
    return searchGalleries?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(FetchGalleriesEvent value)? fetchGalleries,
    TResult Function(SearchGalleriesEvent value)? searchGalleries,
    required TResult orElse(),
  }) {
    if (searchGalleries != null) {
      return searchGalleries(this);
    }
    return orElse();
  }
}

abstract class SearchGalleriesEvent implements GalleryEvent {
  const factory SearchGalleriesEvent({required final String searchQuery}) =
      _$SearchGalleriesEvent;

  String get searchQuery;
  @JsonKey(ignore: true)
  _$$SearchGalleriesEventCopyWith<_$SearchGalleriesEvent> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
mixin _$GalleryState {
  GalleryStatus get status => throw _privateConstructorUsedError;
  List<GalleryEntity> get defaultGalleries =>
      throw _privateConstructorUsedError;
  List<GalleryEntity> get galleries => throw _privateConstructorUsedError;
  String get message => throw _privateConstructorUsedError;

  @JsonKey(ignore: true)
  $GalleryStateCopyWith<GalleryState> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $GalleryStateCopyWith<$Res> {
  factory $GalleryStateCopyWith(
          GalleryState value, $Res Function(GalleryState) then) =
      _$GalleryStateCopyWithImpl<$Res, GalleryState>;
  @useResult
  $Res call(
      {GalleryStatus status,
      List<GalleryEntity> defaultGalleries,
      List<GalleryEntity> galleries,
      String message});
}

/// @nodoc
class _$GalleryStateCopyWithImpl<$Res, $Val extends GalleryState>
    implements $GalleryStateCopyWith<$Res> {
  _$GalleryStateCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? status = null,
    Object? defaultGalleries = null,
    Object? galleries = null,
    Object? message = null,
  }) {
    return _then(_value.copyWith(
      status: null == status
          ? _value.status
          : status // ignore: cast_nullable_to_non_nullable
              as GalleryStatus,
      defaultGalleries: null == defaultGalleries
          ? _value.defaultGalleries
          : defaultGalleries // ignore: cast_nullable_to_non_nullable
              as List<GalleryEntity>,
      galleries: null == galleries
          ? _value.galleries
          : galleries // ignore: cast_nullable_to_non_nullable
              as List<GalleryEntity>,
      message: null == message
          ? _value.message
          : message // ignore: cast_nullable_to_non_nullable
              as String,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$_InitialCopyWith<$Res>
    implements $GalleryStateCopyWith<$Res> {
  factory _$$_InitialCopyWith(
          _$_Initial value, $Res Function(_$_Initial) then) =
      __$$_InitialCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call(
      {GalleryStatus status,
      List<GalleryEntity> defaultGalleries,
      List<GalleryEntity> galleries,
      String message});
}

/// @nodoc
class __$$_InitialCopyWithImpl<$Res>
    extends _$GalleryStateCopyWithImpl<$Res, _$_Initial>
    implements _$$_InitialCopyWith<$Res> {
  __$$_InitialCopyWithImpl(_$_Initial _value, $Res Function(_$_Initial) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? status = null,
    Object? defaultGalleries = null,
    Object? galleries = null,
    Object? message = null,
  }) {
    return _then(_$_Initial(
      status: null == status
          ? _value.status
          : status // ignore: cast_nullable_to_non_nullable
              as GalleryStatus,
      defaultGalleries: null == defaultGalleries
          ? _value._defaultGalleries
          : defaultGalleries // ignore: cast_nullable_to_non_nullable
              as List<GalleryEntity>,
      galleries: null == galleries
          ? _value._galleries
          : galleries // ignore: cast_nullable_to_non_nullable
              as List<GalleryEntity>,
      message: null == message
          ? _value.message
          : message // ignore: cast_nullable_to_non_nullable
              as String,
    ));
  }
}

/// @nodoc

class _$_Initial implements _Initial {
  const _$_Initial(
      {this.status = GalleryStatus.initial,
      final List<GalleryEntity> defaultGalleries = const [],
      final List<GalleryEntity> galleries = const [],
      this.message = ''})
      : _defaultGalleries = defaultGalleries,
        _galleries = galleries;

  @override
  @JsonKey()
  final GalleryStatus status;
  final List<GalleryEntity> _defaultGalleries;
  @override
  @JsonKey()
  List<GalleryEntity> get defaultGalleries {
    // ignore: implicit_dynamic_type
    return EqualUnmodifiableListView(_defaultGalleries);
  }

  final List<GalleryEntity> _galleries;
  @override
  @JsonKey()
  List<GalleryEntity> get galleries {
    // ignore: implicit_dynamic_type
    return EqualUnmodifiableListView(_galleries);
  }

  @override
  @JsonKey()
  final String message;

  @override
  String toString() {
    return 'GalleryState(status: $status, defaultGalleries: $defaultGalleries, galleries: $galleries, message: $message)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_Initial &&
            (identical(other.status, status) || other.status == status) &&
            const DeepCollectionEquality()
                .equals(other._defaultGalleries, _defaultGalleries) &&
            const DeepCollectionEquality()
                .equals(other._galleries, _galleries) &&
            (identical(other.message, message) || other.message == message));
  }

  @override
  int get hashCode => Object.hash(
      runtimeType,
      status,
      const DeepCollectionEquality().hash(_defaultGalleries),
      const DeepCollectionEquality().hash(_galleries),
      message);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$_InitialCopyWith<_$_Initial> get copyWith =>
      __$$_InitialCopyWithImpl<_$_Initial>(this, _$identity);
}

abstract class _Initial implements GalleryState {
  const factory _Initial(
      {final GalleryStatus status,
      final List<GalleryEntity> defaultGalleries,
      final List<GalleryEntity> galleries,
      final String message}) = _$_Initial;

  @override
  GalleryStatus get status;
  @override
  List<GalleryEntity> get defaultGalleries;
  @override
  List<GalleryEntity> get galleries;
  @override
  String get message;
  @override
  @JsonKey(ignore: true)
  _$$_InitialCopyWith<_$_Initial> get copyWith =>
      throw _privateConstructorUsedError;
}
