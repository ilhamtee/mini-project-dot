part of 'gallery_bloc.dart';

@freezed
class GalleryEvent with _$GalleryEvent {
  const factory GalleryEvent.fetchGalleries() = FetchGalleriesEvent;
  const factory GalleryEvent.searchGalleries({required String searchQuery}) =
      SearchGalleriesEvent;
}
