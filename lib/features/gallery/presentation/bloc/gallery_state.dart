part of 'gallery_bloc.dart';

@freezed
class GalleryState with _$GalleryState {
  const factory GalleryState({
    @Default(GalleryStatus.initial) GalleryStatus status,
    @Default([]) List<GalleryEntity> defaultGalleries,
    @Default([]) List<GalleryEntity> galleries,
    @Default('') String message,
  }) = _Initial;
}

enum GalleryStatus {
  initial,
  loading,
  fetchGalleriesSuccess,
  fetchGalleriesError,
  searchGalleriesSuccess
}
