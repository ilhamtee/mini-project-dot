import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:mini_project/cores/widgets/fetch_error_widget.dart';
import 'package:mini_project/cores/widgets/search_widget.dart';
import 'package:mini_project/features/gallery/presentation/bloc/gallery_bloc.dart';
import 'package:mini_project/features/gallery/presentation/widgets/gallery_item_widget.dart';

class GalleryPage extends StatefulWidget {
  const GalleryPage({super.key});

  @override
  State<GalleryPage> createState() => _GalleryPageState();
}

class _GalleryPageState extends State<GalleryPage> {
  late final GalleryBloc _galleryBloc = context.read<GalleryBloc>();

  final TextEditingController _searchController = TextEditingController();
  final FocusNode _searchFocus = FocusNode();

  Timer? _debounce;

  @override
  void dispose() {
    super.dispose();

    _searchController.dispose();
    _searchFocus.dispose();
    _debounce?.cancel();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text(
          'Gallery',
          style: TextStyle(fontWeight: FontWeight.bold),
        ),
        backgroundColor: Colors.green,
        centerTitle: true,
      ),
      body: SafeArea(
          child: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 24),
        child: Column(
          children: [
            SearchWidget(
                searchController: _searchController,
                searchFocus: _searchFocus,
                onChanged: (value) {
                  if (_debounce?.isActive ?? false) _debounce?.cancel();

                  _debounce = Timer(const Duration(milliseconds: 500), () {
                    _galleryBloc.add(SearchGalleriesEvent(searchQuery: value));
                  });
                }),
            const SizedBox(
              height: 16,
            ),
            Expanded(
                child: BlocBuilder<GalleryBloc, GalleryState>(
              bloc: _galleryBloc,
              builder: (_, state) {
                if (state.status == GalleryStatus.loading) {
                  return const Center(
                    child: SizedBox.square(
                      dimension: 26,
                      child: CircularProgressIndicator.adaptive(
                          valueColor:
                              AlwaysStoppedAnimation<Color>(Colors.green)),
                    ),
                  );
                } else if (state.status == GalleryStatus.fetchGalleriesError) {
                  return Center(
                    child: FetchErrorWidget(
                      errorMessage: state.message,
                      onTapReload: () =>
                          _galleryBloc.add(const FetchGalleriesEvent()),
                    ),
                  );
                } else if (state.status ==
                        GalleryStatus.searchGalleriesSuccess &&
                    state.galleries.isEmpty) {
                  return const Center(
                    child: Text('No Found Galleries \nTry Another Gallery Name',
                        textAlign: TextAlign.center),
                  );
                }

                return GridView.builder(
                    gridDelegate:
                        const SliverGridDelegateWithFixedCrossAxisCount(
                            crossAxisCount: 3,
                            crossAxisSpacing: 10,
                            childAspectRatio: 0.80),
                    itemCount: state.galleries.length,
                    itemBuilder: (_, index) {
                      final gallery = state.galleries[index];
                      return GalleryItemWidget(
                        gallery: gallery,
                      );
                    });
              },
            ))
          ],
        ),
      )),
    );
  }
}
