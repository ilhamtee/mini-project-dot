import 'package:flutter/material.dart';
import 'package:mini_project/features/gallery/domain/entities/gallery_entity.dart';

class GalleryItemWidget extends StatelessWidget {
  const GalleryItemWidget({super.key, required this.gallery});

  final GalleryEntity gallery;

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        ClipRRect(
          borderRadius: const BorderRadius.all(Radius.circular(5)),
          child: Image.network(gallery.thumbnail, width: 100, fit: BoxFit.cover,
              errorBuilder: (_, __, stackTrace) {
            return Center(
              child: Text(stackTrace.toString()),
            );
          }, loadingBuilder: (context, child, loadingProgress) {
            if (loadingProgress == null) {
              return child;
            }

            return Container(
              width: 100,
              height: 100,
              alignment: Alignment.center,
              child: const SizedBox.square(
                dimension: 20,
                child: CircularProgressIndicator.adaptive(
                    valueColor: AlwaysStoppedAnimation<Color>(Colors.green)),
              ),
            );
          }),
        ),
        const SizedBox(
          height: 4,
        ),
        Flexible(
            child: Text(
          gallery.caption,
          overflow: TextOverflow.ellipsis,
        )),
      ],
    );
  }
}
