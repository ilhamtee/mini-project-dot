import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:mini_project/features/gallery/presentation/bloc/gallery_bloc.dart';
import 'package:mini_project/features/gallery/presentation/pages/gallery_page.dart';
import 'package:mini_project/features/home/presentation/bloc/home_cubit.dart';
import 'package:mini_project/features/place/presentation/bloc/place_bloc.dart';
import 'package:mini_project/features/place/presentation/pages/place_page.dart';
import 'package:mini_project/features/user_profile/presentation/bloc/profile_bloc.dart';
import 'package:mini_project/features/user_profile/presentation/pages/user_profile_page.dart';

class HomePage extends StatefulWidget {
  const HomePage({super.key});

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  final _listPage = [
    const PlacePage(),
    const GalleryPage(),
    const UserProfilePage(),
  ];

  late final HomeCubit _homeCubit = context.read<HomeCubit>();

  @override
  void initState() {
    context.read<PlaceBloc>().add(const FetchPlacesEvent());
    context.read<GalleryBloc>().add(const FetchGalleriesEvent());
    context.read<ProfileBloc>().add(const FetchUserProfileEvent());

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<HomeCubit, HomeState>(
      bloc: _homeCubit,
      builder: (_, state) {
        return Scaffold(
          body: _listPage[state.selectedPage],
          bottomNavigationBar: BottomNavigationBar(
            currentIndex: state.selectedPage,
            onTap: (index) {
              _homeCubit.changeSelectedPage(index);
            },
            selectedItemColor: Colors.green,
            unselectedItemColor: Colors.green.shade200,
            showUnselectedLabels: false,
            items: const [
              BottomNavigationBarItem(icon: Icon(Icons.place), label: "Place"),
              BottomNavigationBarItem(
                  icon: Icon(Icons.image), label: "Gallery"),
              BottomNavigationBarItem(
                  icon: Icon(Icons.person), label: "Profile"),
            ],
          ),
        );
      },
    );
  }
}
