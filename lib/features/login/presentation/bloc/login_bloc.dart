import 'package:bloc/bloc.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:injectable/injectable.dart';

part 'login_event.dart';
part 'login_state.dart';
part 'login_bloc.freezed.dart';

@injectable
class LoginBloc extends Bloc<LoginEvent, LoginState> {
  LoginBloc() : super(const LoginState()) {
    on<ValidEmailEvent>(_onSetValidEmail);
    on<ValidPasswordEvent>(_onSetValidPassword);
    on<LoginStatusEvent>(_onSetLoginStatus);
  }

  Future<void> _onSetValidEmail(
      ValidEmailEvent event, Emitter<LoginState> emit) async {
    emit(state.copyWith(isValidEmail: event.isValid));
  }

  Future<void> _onSetValidPassword(
      ValidPasswordEvent event, Emitter<LoginState> emit) async {
    emit(state.copyWith(isValidPassword: event.isValid));
  }

  Future<void> _onSetLoginStatus(
      LoginStatusEvent event, Emitter<LoginState> emit) async {
    emit(state.copyWith(loginStatus: event.status));
  }
}
