// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target

part of 'login_bloc.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

/// @nodoc
mixin _$LoginEvent {
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(bool isValid) setValidEmail,
    required TResult Function(bool isValid) setValidPassword,
    required TResult Function(LoginStatus status) setLoginStatus,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function(bool isValid)? setValidEmail,
    TResult? Function(bool isValid)? setValidPassword,
    TResult? Function(LoginStatus status)? setLoginStatus,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(bool isValid)? setValidEmail,
    TResult Function(bool isValid)? setValidPassword,
    TResult Function(LoginStatus status)? setLoginStatus,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(ValidEmailEvent value) setValidEmail,
    required TResult Function(ValidPasswordEvent value) setValidPassword,
    required TResult Function(LoginStatusEvent value) setLoginStatus,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(ValidEmailEvent value)? setValidEmail,
    TResult? Function(ValidPasswordEvent value)? setValidPassword,
    TResult? Function(LoginStatusEvent value)? setLoginStatus,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(ValidEmailEvent value)? setValidEmail,
    TResult Function(ValidPasswordEvent value)? setValidPassword,
    TResult Function(LoginStatusEvent value)? setLoginStatus,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $LoginEventCopyWith<$Res> {
  factory $LoginEventCopyWith(
          LoginEvent value, $Res Function(LoginEvent) then) =
      _$LoginEventCopyWithImpl<$Res, LoginEvent>;
}

/// @nodoc
class _$LoginEventCopyWithImpl<$Res, $Val extends LoginEvent>
    implements $LoginEventCopyWith<$Res> {
  _$LoginEventCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;
}

/// @nodoc
abstract class _$$ValidEmailEventCopyWith<$Res> {
  factory _$$ValidEmailEventCopyWith(
          _$ValidEmailEvent value, $Res Function(_$ValidEmailEvent) then) =
      __$$ValidEmailEventCopyWithImpl<$Res>;
  @useResult
  $Res call({bool isValid});
}

/// @nodoc
class __$$ValidEmailEventCopyWithImpl<$Res>
    extends _$LoginEventCopyWithImpl<$Res, _$ValidEmailEvent>
    implements _$$ValidEmailEventCopyWith<$Res> {
  __$$ValidEmailEventCopyWithImpl(
      _$ValidEmailEvent _value, $Res Function(_$ValidEmailEvent) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? isValid = null,
  }) {
    return _then(_$ValidEmailEvent(
      isValid: null == isValid
          ? _value.isValid
          : isValid // ignore: cast_nullable_to_non_nullable
              as bool,
    ));
  }
}

/// @nodoc

class _$ValidEmailEvent implements ValidEmailEvent {
  const _$ValidEmailEvent({required this.isValid});

  @override
  final bool isValid;

  @override
  String toString() {
    return 'LoginEvent.setValidEmail(isValid: $isValid)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$ValidEmailEvent &&
            (identical(other.isValid, isValid) || other.isValid == isValid));
  }

  @override
  int get hashCode => Object.hash(runtimeType, isValid);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$ValidEmailEventCopyWith<_$ValidEmailEvent> get copyWith =>
      __$$ValidEmailEventCopyWithImpl<_$ValidEmailEvent>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(bool isValid) setValidEmail,
    required TResult Function(bool isValid) setValidPassword,
    required TResult Function(LoginStatus status) setLoginStatus,
  }) {
    return setValidEmail(isValid);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function(bool isValid)? setValidEmail,
    TResult? Function(bool isValid)? setValidPassword,
    TResult? Function(LoginStatus status)? setLoginStatus,
  }) {
    return setValidEmail?.call(isValid);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(bool isValid)? setValidEmail,
    TResult Function(bool isValid)? setValidPassword,
    TResult Function(LoginStatus status)? setLoginStatus,
    required TResult orElse(),
  }) {
    if (setValidEmail != null) {
      return setValidEmail(isValid);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(ValidEmailEvent value) setValidEmail,
    required TResult Function(ValidPasswordEvent value) setValidPassword,
    required TResult Function(LoginStatusEvent value) setLoginStatus,
  }) {
    return setValidEmail(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(ValidEmailEvent value)? setValidEmail,
    TResult? Function(ValidPasswordEvent value)? setValidPassword,
    TResult? Function(LoginStatusEvent value)? setLoginStatus,
  }) {
    return setValidEmail?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(ValidEmailEvent value)? setValidEmail,
    TResult Function(ValidPasswordEvent value)? setValidPassword,
    TResult Function(LoginStatusEvent value)? setLoginStatus,
    required TResult orElse(),
  }) {
    if (setValidEmail != null) {
      return setValidEmail(this);
    }
    return orElse();
  }
}

abstract class ValidEmailEvent implements LoginEvent {
  const factory ValidEmailEvent({required final bool isValid}) =
      _$ValidEmailEvent;

  bool get isValid;
  @JsonKey(ignore: true)
  _$$ValidEmailEventCopyWith<_$ValidEmailEvent> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$$ValidPasswordEventCopyWith<$Res> {
  factory _$$ValidPasswordEventCopyWith(_$ValidPasswordEvent value,
          $Res Function(_$ValidPasswordEvent) then) =
      __$$ValidPasswordEventCopyWithImpl<$Res>;
  @useResult
  $Res call({bool isValid});
}

/// @nodoc
class __$$ValidPasswordEventCopyWithImpl<$Res>
    extends _$LoginEventCopyWithImpl<$Res, _$ValidPasswordEvent>
    implements _$$ValidPasswordEventCopyWith<$Res> {
  __$$ValidPasswordEventCopyWithImpl(
      _$ValidPasswordEvent _value, $Res Function(_$ValidPasswordEvent) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? isValid = null,
  }) {
    return _then(_$ValidPasswordEvent(
      isValid: null == isValid
          ? _value.isValid
          : isValid // ignore: cast_nullable_to_non_nullable
              as bool,
    ));
  }
}

/// @nodoc

class _$ValidPasswordEvent implements ValidPasswordEvent {
  const _$ValidPasswordEvent({required this.isValid});

  @override
  final bool isValid;

  @override
  String toString() {
    return 'LoginEvent.setValidPassword(isValid: $isValid)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$ValidPasswordEvent &&
            (identical(other.isValid, isValid) || other.isValid == isValid));
  }

  @override
  int get hashCode => Object.hash(runtimeType, isValid);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$ValidPasswordEventCopyWith<_$ValidPasswordEvent> get copyWith =>
      __$$ValidPasswordEventCopyWithImpl<_$ValidPasswordEvent>(
          this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(bool isValid) setValidEmail,
    required TResult Function(bool isValid) setValidPassword,
    required TResult Function(LoginStatus status) setLoginStatus,
  }) {
    return setValidPassword(isValid);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function(bool isValid)? setValidEmail,
    TResult? Function(bool isValid)? setValidPassword,
    TResult? Function(LoginStatus status)? setLoginStatus,
  }) {
    return setValidPassword?.call(isValid);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(bool isValid)? setValidEmail,
    TResult Function(bool isValid)? setValidPassword,
    TResult Function(LoginStatus status)? setLoginStatus,
    required TResult orElse(),
  }) {
    if (setValidPassword != null) {
      return setValidPassword(isValid);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(ValidEmailEvent value) setValidEmail,
    required TResult Function(ValidPasswordEvent value) setValidPassword,
    required TResult Function(LoginStatusEvent value) setLoginStatus,
  }) {
    return setValidPassword(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(ValidEmailEvent value)? setValidEmail,
    TResult? Function(ValidPasswordEvent value)? setValidPassword,
    TResult? Function(LoginStatusEvent value)? setLoginStatus,
  }) {
    return setValidPassword?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(ValidEmailEvent value)? setValidEmail,
    TResult Function(ValidPasswordEvent value)? setValidPassword,
    TResult Function(LoginStatusEvent value)? setLoginStatus,
    required TResult orElse(),
  }) {
    if (setValidPassword != null) {
      return setValidPassword(this);
    }
    return orElse();
  }
}

abstract class ValidPasswordEvent implements LoginEvent {
  const factory ValidPasswordEvent({required final bool isValid}) =
      _$ValidPasswordEvent;

  bool get isValid;
  @JsonKey(ignore: true)
  _$$ValidPasswordEventCopyWith<_$ValidPasswordEvent> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$$LoginStatusEventCopyWith<$Res> {
  factory _$$LoginStatusEventCopyWith(
          _$LoginStatusEvent value, $Res Function(_$LoginStatusEvent) then) =
      __$$LoginStatusEventCopyWithImpl<$Res>;
  @useResult
  $Res call({LoginStatus status});
}

/// @nodoc
class __$$LoginStatusEventCopyWithImpl<$Res>
    extends _$LoginEventCopyWithImpl<$Res, _$LoginStatusEvent>
    implements _$$LoginStatusEventCopyWith<$Res> {
  __$$LoginStatusEventCopyWithImpl(
      _$LoginStatusEvent _value, $Res Function(_$LoginStatusEvent) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? status = null,
  }) {
    return _then(_$LoginStatusEvent(
      status: null == status
          ? _value.status
          : status // ignore: cast_nullable_to_non_nullable
              as LoginStatus,
    ));
  }
}

/// @nodoc

class _$LoginStatusEvent implements LoginStatusEvent {
  const _$LoginStatusEvent({required this.status});

  @override
  final LoginStatus status;

  @override
  String toString() {
    return 'LoginEvent.setLoginStatus(status: $status)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$LoginStatusEvent &&
            (identical(other.status, status) || other.status == status));
  }

  @override
  int get hashCode => Object.hash(runtimeType, status);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$LoginStatusEventCopyWith<_$LoginStatusEvent> get copyWith =>
      __$$LoginStatusEventCopyWithImpl<_$LoginStatusEvent>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(bool isValid) setValidEmail,
    required TResult Function(bool isValid) setValidPassword,
    required TResult Function(LoginStatus status) setLoginStatus,
  }) {
    return setLoginStatus(status);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function(bool isValid)? setValidEmail,
    TResult? Function(bool isValid)? setValidPassword,
    TResult? Function(LoginStatus status)? setLoginStatus,
  }) {
    return setLoginStatus?.call(status);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(bool isValid)? setValidEmail,
    TResult Function(bool isValid)? setValidPassword,
    TResult Function(LoginStatus status)? setLoginStatus,
    required TResult orElse(),
  }) {
    if (setLoginStatus != null) {
      return setLoginStatus(status);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(ValidEmailEvent value) setValidEmail,
    required TResult Function(ValidPasswordEvent value) setValidPassword,
    required TResult Function(LoginStatusEvent value) setLoginStatus,
  }) {
    return setLoginStatus(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(ValidEmailEvent value)? setValidEmail,
    TResult? Function(ValidPasswordEvent value)? setValidPassword,
    TResult? Function(LoginStatusEvent value)? setLoginStatus,
  }) {
    return setLoginStatus?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(ValidEmailEvent value)? setValidEmail,
    TResult Function(ValidPasswordEvent value)? setValidPassword,
    TResult Function(LoginStatusEvent value)? setLoginStatus,
    required TResult orElse(),
  }) {
    if (setLoginStatus != null) {
      return setLoginStatus(this);
    }
    return orElse();
  }
}

abstract class LoginStatusEvent implements LoginEvent {
  const factory LoginStatusEvent({required final LoginStatus status}) =
      _$LoginStatusEvent;

  LoginStatus get status;
  @JsonKey(ignore: true)
  _$$LoginStatusEventCopyWith<_$LoginStatusEvent> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
mixin _$LoginState {
  LoginStatus get loginStatus => throw _privateConstructorUsedError;
  bool get isValidEmail => throw _privateConstructorUsedError;
  bool get isValidPassword => throw _privateConstructorUsedError;

  @JsonKey(ignore: true)
  $LoginStateCopyWith<LoginState> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $LoginStateCopyWith<$Res> {
  factory $LoginStateCopyWith(
          LoginState value, $Res Function(LoginState) then) =
      _$LoginStateCopyWithImpl<$Res, LoginState>;
  @useResult
  $Res call({LoginStatus loginStatus, bool isValidEmail, bool isValidPassword});
}

/// @nodoc
class _$LoginStateCopyWithImpl<$Res, $Val extends LoginState>
    implements $LoginStateCopyWith<$Res> {
  _$LoginStateCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? loginStatus = null,
    Object? isValidEmail = null,
    Object? isValidPassword = null,
  }) {
    return _then(_value.copyWith(
      loginStatus: null == loginStatus
          ? _value.loginStatus
          : loginStatus // ignore: cast_nullable_to_non_nullable
              as LoginStatus,
      isValidEmail: null == isValidEmail
          ? _value.isValidEmail
          : isValidEmail // ignore: cast_nullable_to_non_nullable
              as bool,
      isValidPassword: null == isValidPassword
          ? _value.isValidPassword
          : isValidPassword // ignore: cast_nullable_to_non_nullable
              as bool,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$_LoginStateCopyWith<$Res>
    implements $LoginStateCopyWith<$Res> {
  factory _$$_LoginStateCopyWith(
          _$_LoginState value, $Res Function(_$_LoginState) then) =
      __$$_LoginStateCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call({LoginStatus loginStatus, bool isValidEmail, bool isValidPassword});
}

/// @nodoc
class __$$_LoginStateCopyWithImpl<$Res>
    extends _$LoginStateCopyWithImpl<$Res, _$_LoginState>
    implements _$$_LoginStateCopyWith<$Res> {
  __$$_LoginStateCopyWithImpl(
      _$_LoginState _value, $Res Function(_$_LoginState) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? loginStatus = null,
    Object? isValidEmail = null,
    Object? isValidPassword = null,
  }) {
    return _then(_$_LoginState(
      loginStatus: null == loginStatus
          ? _value.loginStatus
          : loginStatus // ignore: cast_nullable_to_non_nullable
              as LoginStatus,
      isValidEmail: null == isValidEmail
          ? _value.isValidEmail
          : isValidEmail // ignore: cast_nullable_to_non_nullable
              as bool,
      isValidPassword: null == isValidPassword
          ? _value.isValidPassword
          : isValidPassword // ignore: cast_nullable_to_non_nullable
              as bool,
    ));
  }
}

/// @nodoc

class _$_LoginState implements _LoginState {
  const _$_LoginState(
      {this.loginStatus = LoginStatus.initial,
      this.isValidEmail = false,
      this.isValidPassword = false});

  @override
  @JsonKey()
  final LoginStatus loginStatus;
  @override
  @JsonKey()
  final bool isValidEmail;
  @override
  @JsonKey()
  final bool isValidPassword;

  @override
  String toString() {
    return 'LoginState(loginStatus: $loginStatus, isValidEmail: $isValidEmail, isValidPassword: $isValidPassword)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_LoginState &&
            (identical(other.loginStatus, loginStatus) ||
                other.loginStatus == loginStatus) &&
            (identical(other.isValidEmail, isValidEmail) ||
                other.isValidEmail == isValidEmail) &&
            (identical(other.isValidPassword, isValidPassword) ||
                other.isValidPassword == isValidPassword));
  }

  @override
  int get hashCode =>
      Object.hash(runtimeType, loginStatus, isValidEmail, isValidPassword);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$_LoginStateCopyWith<_$_LoginState> get copyWith =>
      __$$_LoginStateCopyWithImpl<_$_LoginState>(this, _$identity);
}

abstract class _LoginState implements LoginState {
  const factory _LoginState(
      {final LoginStatus loginStatus,
      final bool isValidEmail,
      final bool isValidPassword}) = _$_LoginState;

  @override
  LoginStatus get loginStatus;
  @override
  bool get isValidEmail;
  @override
  bool get isValidPassword;
  @override
  @JsonKey(ignore: true)
  _$$_LoginStateCopyWith<_$_LoginState> get copyWith =>
      throw _privateConstructorUsedError;
}
