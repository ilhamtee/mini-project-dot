part of 'login_bloc.dart';

@freezed
class LoginEvent with _$LoginEvent {
  const factory LoginEvent.setValidEmail({required bool isValid}) =
      ValidEmailEvent;
  const factory LoginEvent.setValidPassword({required bool isValid}) =
      ValidPasswordEvent;
  const factory LoginEvent.setLoginStatus({required LoginStatus status}) =
      LoginStatusEvent;
}
