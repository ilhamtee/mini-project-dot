part of 'login_bloc.dart';

@freezed
class LoginState with _$LoginState {
  const factory LoginState({
    @Default(LoginStatus.initial) LoginStatus loginStatus,
    @Default(false) bool isValidEmail,
    @Default(false) bool isValidPassword,
  }) = _LoginState;
}

enum LoginStatus {
  initial,
  loading,
  loginSuccess,
  loginFailed;
}
