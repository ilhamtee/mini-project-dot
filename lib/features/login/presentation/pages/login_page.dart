import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:mini_project/cores/constants/preferences_constant.dart';
import 'package:mini_project/cores/extensions/string_extension.dart';
import 'package:mini_project/cores/widgets/app_text_form_field_widget.dart';
import 'package:mini_project/features/home/presentation/pages/home_page.dart';
import 'package:mini_project/features/login/presentation/bloc/login_bloc.dart';
import 'package:mini_project/injector.dart';
import 'package:shared_preferences/shared_preferences.dart';

class LoginPage extends StatefulWidget {
  const LoginPage({super.key});

  @override
  State<LoginPage> createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  final TextEditingController _emailController = TextEditingController();
  final TextEditingController _passwordController = TextEditingController();
  final FocusNode _emailFocus = FocusNode();
  final FocusNode _passwordFocus = FocusNode();

  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();

  late final LoginBloc _loginBloc = context.read<LoginBloc>();

  @override
  void dispose() {
    super.dispose();
    _emailController.dispose();
    _passwordController.dispose();
    _emailFocus.dispose();
    _passwordFocus.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: SafeArea(
        child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 20),
          child: Form(
            key: _formKey,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                const Center(
                    child: Text(
                  'LOGIN',
                  style: TextStyle(
                      color: Colors.green,
                      fontSize: 24,
                      fontWeight: FontWeight.bold),
                )),
                const SizedBox(
                  height: 40,
                ),
                AppTextFormField(
                  controller: _emailController,
                  focusNode: _emailFocus,
                  keyboardType: TextInputType.emailAddress,
                  autoValidateMode: AutovalidateMode.onUserInteraction,
                  validator: (value) {
                    if (value != null) {
                      if (value.isValidEmail) {
                        _loginBloc.add(const ValidEmailEvent(isValid: true));
                        return null;
                      }

                      _loginBloc.add(const ValidEmailEvent(isValid: false));
                      return 'Format email salah';
                    }
                    return null;
                  },
                  hintText: 'Email',
                ),
                const SizedBox(
                  height: 16,
                ),
                AppTextFormField(
                  controller: _passwordController,
                  focusNode: _passwordFocus,
                  isPassword: true,
                  autoValidateMode: AutovalidateMode.onUserInteraction,
                  validator: (value) {
                    if (value != null) {
                      if (value.isValidPassword) {
                        _loginBloc.add(const ValidPasswordEvent(isValid: true));
                        return null;
                      }

                      _loginBloc.add(const ValidPasswordEvent(isValid: false));
                      return 'Password min 6 karakter, terdapat minimal 1 karakter \nuppercase dan lowercase';
                    }
                    return null;
                  },
                  hintText: 'Password',
                ),
                const SizedBox(
                  height: 60,
                ),
                BlocBuilder<LoginBloc, LoginState>(
                  bloc: _loginBloc,
                  builder: (_, state) {
                    return SizedBox(
                      width: double.infinity,
                      child: ElevatedButton(
                        onPressed: state.isValidEmail && state.isValidPassword
                            ? () async {
                                if (state.loginStatus != LoginStatus.loading) {
                                  _emailFocus.unfocus();
                                  _passwordFocus.unfocus();
                                  _loginBloc.add(const LoginStatusEvent(
                                      status: LoginStatus.loading));

                                  await Future.delayed(
                                      const Duration(seconds: 3), () {
                                    getIt<SharedPreferences>().setBool(
                                        PreferencesConstant.isLogin, true);
                                    Navigator.pushReplacement(
                                        context,
                                        MaterialPageRoute(
                                            builder: (_) => const HomePage()));
                                  });
                                }
                              }
                            : null,
                        style: ElevatedButton.styleFrom(
                            backgroundColor: Colors.green),
                        child: state.loginStatus == LoginStatus.loading
                            ? const SizedBox.square(
                                dimension: 20.0,
                                child: CircularProgressIndicator.adaptive(
                                    valueColor: AlwaysStoppedAnimation<Color>(
                                        Colors.white)),
                              )
                            : const Text('Submit'),
                      ),
                    );
                  },
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}
