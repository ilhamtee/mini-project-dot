import 'package:dartz/dartz.dart';
import 'package:mini_project/cores/exception/exception.dart';
import 'package:mini_project/cores/use_case/use_case.dart';
import 'package:mini_project/features/place/data/models/place_model.dart';

abstract class PlaceRemoteDataSource {
  Future<Either<Failures, List<PlaceModel>>> getPlaces(
    NoParams noParams,
  );
}
