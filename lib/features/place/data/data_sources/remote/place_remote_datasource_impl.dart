import 'package:dartz/dartz.dart';
import 'package:dio/dio.dart';
import 'package:injectable/injectable.dart';
import 'package:mini_project/cores/constants/url_constant.dart';
import 'package:mini_project/cores/exception/exception.dart';
import 'package:mini_project/cores/extensions/dio_response_extension.dart';
import 'package:mini_project/cores/helpers/dio_helper.dart';
import 'package:mini_project/cores/use_case/use_case.dart';
import 'package:mini_project/features/place/data/data_sources/remote/place_remote_datasource.dart';
import 'package:mini_project/features/place/data/models/place_model.dart';

@LazySingleton(as: PlaceRemoteDataSource)
class PlaceRemoteDataSourceImpl implements PlaceRemoteDataSource {
  final Dio _dio;

  PlaceRemoteDataSourceImpl(this._dio);

  @override
  Future<Either<Failures, List<PlaceModel>>> getPlaces(
      NoParams noParams) async {
    try {
      final response = await _dio.get(
        URLConstant.getPlaces,
      );

      if (response.isOk) {
        final List<dynamic> rawPlacesData = response.data['data']['content'];

        final List<PlaceModel> mappedPlacesData =
            rawPlacesData.map((e) => PlaceModel.fromJson(e)).toList();

        return Right(mappedPlacesData);
      }

      return const Right([]);
    } on DioError catch (e) {
      final message = DioHelper.generalException(e);
      return Left(ServerFailure(errorMessage: message));
    }
  }
}
