import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:mini_project/features/place/domain/entities/place_entity.dart';

part 'place_model.freezed.dart';
part 'place_model.g.dart';

@freezed
class PlaceModel with _$PlaceModel {
  factory PlaceModel({
    required int id,
    required String title,
    required String content,
    required PlaceType type,
    String? image,
    required List<String> media,
  }) = _PlaceModel;

  factory PlaceModel.fromJson(Map<String, dynamic> json) =>
      _$PlaceModelFromJson(json);
}

enum PlaceType {
  image,
  multiple;
}

extension PlaceModelX on PlaceModel {
  PlaceEntity toDomain() => PlaceEntity(
      id: id,
      title: title,
      content: content,
      type: type,
      media: media,
      image: image);
}
