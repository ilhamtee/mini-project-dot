// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'place_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$_PlaceModel _$$_PlaceModelFromJson(Map<String, dynamic> json) =>
    _$_PlaceModel(
      id: json['id'] as int,
      title: json['title'] as String,
      content: json['content'] as String,
      type: $enumDecode(_$PlaceTypeEnumMap, json['type']),
      image: json['image'] as String?,
      media: (json['media'] as List<dynamic>).map((e) => e as String).toList(),
    );

Map<String, dynamic> _$$_PlaceModelToJson(_$_PlaceModel instance) =>
    <String, dynamic>{
      'id': instance.id,
      'title': instance.title,
      'content': instance.content,
      'type': _$PlaceTypeEnumMap[instance.type]!,
      'image': instance.image,
      'media': instance.media,
    };

const _$PlaceTypeEnumMap = {
  PlaceType.image: 'image',
  PlaceType.multiple: 'multiple',
};
