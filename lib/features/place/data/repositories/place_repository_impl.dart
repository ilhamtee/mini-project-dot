import 'package:dartz/dartz.dart';
import 'package:injectable/injectable.dart';
import 'package:mini_project/cores/exception/exception.dart';
import 'package:mini_project/cores/use_case/use_case.dart';
import 'package:mini_project/features/place/data/data_sources/remote/place_remote_datasource.dart';
import 'package:mini_project/features/place/data/models/place_model.dart';
import 'package:mini_project/features/place/domain/repositories/place_repository.dart';

@LazySingleton(as: PlaceRepository)
class PlaceRepositoryImpl implements PlaceRepository {
  PlaceRepositoryImpl(this._remoteDataSource);

  final PlaceRemoteDataSource _remoteDataSource;

  @override
  Future<Either<Failures, List<PlaceModel>>> getPlaces(
      NoParams noParams) async {
    return await _remoteDataSource.getPlaces(noParams);
  }
}
