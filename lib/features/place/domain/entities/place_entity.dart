import 'package:mini_project/features/place/data/models/place_model.dart';

import 'package:equatable/equatable.dart';

class PlaceEntity extends Equatable {
  final int id;
  final String title;
  final String content;
  final PlaceType type;
  final String? image;
  final List<String> media;

  const PlaceEntity({
    required this.id,
    required this.title,
    required this.content,
    required this.type,
    this.image,
    required this.media,
  });

  @override
  List<Object?> get props => [id, title, content, type, image, media];
}
