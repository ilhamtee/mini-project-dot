import 'package:dartz/dartz.dart';
import 'package:injectable/injectable.dart';
import 'package:mini_project/cores/exception/exception.dart';
import 'package:mini_project/cores/use_case/use_case.dart';
import 'package:mini_project/features/place/data/models/place_model.dart';
import 'package:mini_project/features/place/domain/repositories/place_repository.dart';

@injectable
class FetchPlacesUseCase implements UseCase<List<PlaceModel>, NoParams> {
  const FetchPlacesUseCase(this._repository);
  final PlaceRepository _repository;

  @override
  Future<Either<Failures, List<PlaceModel>>> call(
    NoParams params,
  ) async {
    return await _repository.getPlaces(params);
  }
}
