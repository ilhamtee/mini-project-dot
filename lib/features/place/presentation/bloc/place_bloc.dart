import 'package:bloc/bloc.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:injectable/injectable.dart';
import 'package:mini_project/cores/use_case/use_case.dart';
import 'package:mini_project/features/place/data/models/place_model.dart';
import 'package:mini_project/features/place/domain/entities/place_entity.dart';
import 'package:mini_project/features/place/domain/use_cases/fetch_places_use_case.dart';

part 'place_event.dart';
part 'place_state.dart';
part 'place_bloc.freezed.dart';

@injectable
class PlaceBloc extends Bloc<PlaceEvent, PlaceState> {
  PlaceBloc(this._fetchPlacesUseCase) : super(const PlaceState()) {
    on<FetchPlacesEvent>(_onFetchPlaces);
    on<SearchPlacesEvent>(_onSearchPlaces);
  }

  final FetchPlacesUseCase _fetchPlacesUseCase;

  Future<void> _onFetchPlaces(
      FetchPlacesEvent event, Emitter<PlaceState> emit) async {
    emit(state.copyWith(status: PlaceStatus.loading));

    final fetchPlacesResult = await _fetchPlacesUseCase(NoParams());

    fetchPlacesResult.fold(
        (failed) => emit(state.copyWith(
            message: failed.errorMessage,
            status: PlaceStatus.fetchPlacesError)),
        (placesModel) => emit(state.copyWith(
            places: placesModel.map((e) => e.toDomain()).toList(),
            defaultPlaces: placesModel.map((e) => e.toDomain()).toList(),
            status: PlaceStatus.fetchPlacesSuccess)));
  }

  Future<void> _onSearchPlaces(
      SearchPlacesEvent event, Emitter<PlaceState> emit) async {
    emit(state.copyWith(status: PlaceStatus.loading));

    final List<PlaceEntity> defaultPlaces = state.defaultPlaces;

    if (event.searchQuery.isNotEmpty) {
      final List<PlaceEntity> filteredPlaces = defaultPlaces
          .where((place) => place.title
              .toLowerCase()
              .contains(event.searchQuery.toLowerCase()))
          .toList();

      emit(state.copyWith(
          status: PlaceStatus.searchPlacesSuccess, places: filteredPlaces));
    } else {
      emit(state.copyWith(
          status: PlaceStatus.searchPlacesSuccess, places: defaultPlaces));
    }
  }
}
