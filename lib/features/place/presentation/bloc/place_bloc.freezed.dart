// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target

part of 'place_bloc.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

/// @nodoc
mixin _$PlaceEvent {
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() fetchPlaces,
    required TResult Function(String searchQuery) searchPlaces,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? fetchPlaces,
    TResult? Function(String searchQuery)? searchPlaces,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? fetchPlaces,
    TResult Function(String searchQuery)? searchPlaces,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(FetchPlacesEvent value) fetchPlaces,
    required TResult Function(SearchPlacesEvent value) searchPlaces,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(FetchPlacesEvent value)? fetchPlaces,
    TResult? Function(SearchPlacesEvent value)? searchPlaces,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(FetchPlacesEvent value)? fetchPlaces,
    TResult Function(SearchPlacesEvent value)? searchPlaces,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $PlaceEventCopyWith<$Res> {
  factory $PlaceEventCopyWith(
          PlaceEvent value, $Res Function(PlaceEvent) then) =
      _$PlaceEventCopyWithImpl<$Res, PlaceEvent>;
}

/// @nodoc
class _$PlaceEventCopyWithImpl<$Res, $Val extends PlaceEvent>
    implements $PlaceEventCopyWith<$Res> {
  _$PlaceEventCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;
}

/// @nodoc
abstract class _$$FetchPlacesEventCopyWith<$Res> {
  factory _$$FetchPlacesEventCopyWith(
          _$FetchPlacesEvent value, $Res Function(_$FetchPlacesEvent) then) =
      __$$FetchPlacesEventCopyWithImpl<$Res>;
}

/// @nodoc
class __$$FetchPlacesEventCopyWithImpl<$Res>
    extends _$PlaceEventCopyWithImpl<$Res, _$FetchPlacesEvent>
    implements _$$FetchPlacesEventCopyWith<$Res> {
  __$$FetchPlacesEventCopyWithImpl(
      _$FetchPlacesEvent _value, $Res Function(_$FetchPlacesEvent) _then)
      : super(_value, _then);
}

/// @nodoc

class _$FetchPlacesEvent implements FetchPlacesEvent {
  const _$FetchPlacesEvent();

  @override
  String toString() {
    return 'PlaceEvent.fetchPlaces()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$FetchPlacesEvent);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() fetchPlaces,
    required TResult Function(String searchQuery) searchPlaces,
  }) {
    return fetchPlaces();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? fetchPlaces,
    TResult? Function(String searchQuery)? searchPlaces,
  }) {
    return fetchPlaces?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? fetchPlaces,
    TResult Function(String searchQuery)? searchPlaces,
    required TResult orElse(),
  }) {
    if (fetchPlaces != null) {
      return fetchPlaces();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(FetchPlacesEvent value) fetchPlaces,
    required TResult Function(SearchPlacesEvent value) searchPlaces,
  }) {
    return fetchPlaces(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(FetchPlacesEvent value)? fetchPlaces,
    TResult? Function(SearchPlacesEvent value)? searchPlaces,
  }) {
    return fetchPlaces?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(FetchPlacesEvent value)? fetchPlaces,
    TResult Function(SearchPlacesEvent value)? searchPlaces,
    required TResult orElse(),
  }) {
    if (fetchPlaces != null) {
      return fetchPlaces(this);
    }
    return orElse();
  }
}

abstract class FetchPlacesEvent implements PlaceEvent {
  const factory FetchPlacesEvent() = _$FetchPlacesEvent;
}

/// @nodoc
abstract class _$$SearchPlacesEventCopyWith<$Res> {
  factory _$$SearchPlacesEventCopyWith(
          _$SearchPlacesEvent value, $Res Function(_$SearchPlacesEvent) then) =
      __$$SearchPlacesEventCopyWithImpl<$Res>;
  @useResult
  $Res call({String searchQuery});
}

/// @nodoc
class __$$SearchPlacesEventCopyWithImpl<$Res>
    extends _$PlaceEventCopyWithImpl<$Res, _$SearchPlacesEvent>
    implements _$$SearchPlacesEventCopyWith<$Res> {
  __$$SearchPlacesEventCopyWithImpl(
      _$SearchPlacesEvent _value, $Res Function(_$SearchPlacesEvent) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? searchQuery = null,
  }) {
    return _then(_$SearchPlacesEvent(
      searchQuery: null == searchQuery
          ? _value.searchQuery
          : searchQuery // ignore: cast_nullable_to_non_nullable
              as String,
    ));
  }
}

/// @nodoc

class _$SearchPlacesEvent implements SearchPlacesEvent {
  const _$SearchPlacesEvent({required this.searchQuery});

  @override
  final String searchQuery;

  @override
  String toString() {
    return 'PlaceEvent.searchPlaces(searchQuery: $searchQuery)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$SearchPlacesEvent &&
            (identical(other.searchQuery, searchQuery) ||
                other.searchQuery == searchQuery));
  }

  @override
  int get hashCode => Object.hash(runtimeType, searchQuery);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$SearchPlacesEventCopyWith<_$SearchPlacesEvent> get copyWith =>
      __$$SearchPlacesEventCopyWithImpl<_$SearchPlacesEvent>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() fetchPlaces,
    required TResult Function(String searchQuery) searchPlaces,
  }) {
    return searchPlaces(searchQuery);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? fetchPlaces,
    TResult? Function(String searchQuery)? searchPlaces,
  }) {
    return searchPlaces?.call(searchQuery);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? fetchPlaces,
    TResult Function(String searchQuery)? searchPlaces,
    required TResult orElse(),
  }) {
    if (searchPlaces != null) {
      return searchPlaces(searchQuery);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(FetchPlacesEvent value) fetchPlaces,
    required TResult Function(SearchPlacesEvent value) searchPlaces,
  }) {
    return searchPlaces(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(FetchPlacesEvent value)? fetchPlaces,
    TResult? Function(SearchPlacesEvent value)? searchPlaces,
  }) {
    return searchPlaces?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(FetchPlacesEvent value)? fetchPlaces,
    TResult Function(SearchPlacesEvent value)? searchPlaces,
    required TResult orElse(),
  }) {
    if (searchPlaces != null) {
      return searchPlaces(this);
    }
    return orElse();
  }
}

abstract class SearchPlacesEvent implements PlaceEvent {
  const factory SearchPlacesEvent({required final String searchQuery}) =
      _$SearchPlacesEvent;

  String get searchQuery;
  @JsonKey(ignore: true)
  _$$SearchPlacesEventCopyWith<_$SearchPlacesEvent> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
mixin _$PlaceState {
  PlaceStatus get status => throw _privateConstructorUsedError;
  List<PlaceEntity> get defaultPlaces => throw _privateConstructorUsedError;
  List<PlaceEntity> get places => throw _privateConstructorUsedError;
  String get message => throw _privateConstructorUsedError;

  @JsonKey(ignore: true)
  $PlaceStateCopyWith<PlaceState> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $PlaceStateCopyWith<$Res> {
  factory $PlaceStateCopyWith(
          PlaceState value, $Res Function(PlaceState) then) =
      _$PlaceStateCopyWithImpl<$Res, PlaceState>;
  @useResult
  $Res call(
      {PlaceStatus status,
      List<PlaceEntity> defaultPlaces,
      List<PlaceEntity> places,
      String message});
}

/// @nodoc
class _$PlaceStateCopyWithImpl<$Res, $Val extends PlaceState>
    implements $PlaceStateCopyWith<$Res> {
  _$PlaceStateCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? status = null,
    Object? defaultPlaces = null,
    Object? places = null,
    Object? message = null,
  }) {
    return _then(_value.copyWith(
      status: null == status
          ? _value.status
          : status // ignore: cast_nullable_to_non_nullable
              as PlaceStatus,
      defaultPlaces: null == defaultPlaces
          ? _value.defaultPlaces
          : defaultPlaces // ignore: cast_nullable_to_non_nullable
              as List<PlaceEntity>,
      places: null == places
          ? _value.places
          : places // ignore: cast_nullable_to_non_nullable
              as List<PlaceEntity>,
      message: null == message
          ? _value.message
          : message // ignore: cast_nullable_to_non_nullable
              as String,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$_InitialCopyWith<$Res> implements $PlaceStateCopyWith<$Res> {
  factory _$$_InitialCopyWith(
          _$_Initial value, $Res Function(_$_Initial) then) =
      __$$_InitialCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call(
      {PlaceStatus status,
      List<PlaceEntity> defaultPlaces,
      List<PlaceEntity> places,
      String message});
}

/// @nodoc
class __$$_InitialCopyWithImpl<$Res>
    extends _$PlaceStateCopyWithImpl<$Res, _$_Initial>
    implements _$$_InitialCopyWith<$Res> {
  __$$_InitialCopyWithImpl(_$_Initial _value, $Res Function(_$_Initial) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? status = null,
    Object? defaultPlaces = null,
    Object? places = null,
    Object? message = null,
  }) {
    return _then(_$_Initial(
      status: null == status
          ? _value.status
          : status // ignore: cast_nullable_to_non_nullable
              as PlaceStatus,
      defaultPlaces: null == defaultPlaces
          ? _value._defaultPlaces
          : defaultPlaces // ignore: cast_nullable_to_non_nullable
              as List<PlaceEntity>,
      places: null == places
          ? _value._places
          : places // ignore: cast_nullable_to_non_nullable
              as List<PlaceEntity>,
      message: null == message
          ? _value.message
          : message // ignore: cast_nullable_to_non_nullable
              as String,
    ));
  }
}

/// @nodoc

class _$_Initial implements _Initial {
  const _$_Initial(
      {this.status = PlaceStatus.initial,
      final List<PlaceEntity> defaultPlaces = const [],
      final List<PlaceEntity> places = const [],
      this.message = ''})
      : _defaultPlaces = defaultPlaces,
        _places = places;

  @override
  @JsonKey()
  final PlaceStatus status;
  final List<PlaceEntity> _defaultPlaces;
  @override
  @JsonKey()
  List<PlaceEntity> get defaultPlaces {
    // ignore: implicit_dynamic_type
    return EqualUnmodifiableListView(_defaultPlaces);
  }

  final List<PlaceEntity> _places;
  @override
  @JsonKey()
  List<PlaceEntity> get places {
    // ignore: implicit_dynamic_type
    return EqualUnmodifiableListView(_places);
  }

  @override
  @JsonKey()
  final String message;

  @override
  String toString() {
    return 'PlaceState(status: $status, defaultPlaces: $defaultPlaces, places: $places, message: $message)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_Initial &&
            (identical(other.status, status) || other.status == status) &&
            const DeepCollectionEquality()
                .equals(other._defaultPlaces, _defaultPlaces) &&
            const DeepCollectionEquality().equals(other._places, _places) &&
            (identical(other.message, message) || other.message == message));
  }

  @override
  int get hashCode => Object.hash(
      runtimeType,
      status,
      const DeepCollectionEquality().hash(_defaultPlaces),
      const DeepCollectionEquality().hash(_places),
      message);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$_InitialCopyWith<_$_Initial> get copyWith =>
      __$$_InitialCopyWithImpl<_$_Initial>(this, _$identity);
}

abstract class _Initial implements PlaceState {
  const factory _Initial(
      {final PlaceStatus status,
      final List<PlaceEntity> defaultPlaces,
      final List<PlaceEntity> places,
      final String message}) = _$_Initial;

  @override
  PlaceStatus get status;
  @override
  List<PlaceEntity> get defaultPlaces;
  @override
  List<PlaceEntity> get places;
  @override
  String get message;
  @override
  @JsonKey(ignore: true)
  _$$_InitialCopyWith<_$_Initial> get copyWith =>
      throw _privateConstructorUsedError;
}
