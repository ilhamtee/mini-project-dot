part of 'place_bloc.dart';

@freezed
class PlaceEvent with _$PlaceEvent {
  const factory PlaceEvent.fetchPlaces() = FetchPlacesEvent;
  const factory PlaceEvent.searchPlaces({required String searchQuery}) =
      SearchPlacesEvent;
}
