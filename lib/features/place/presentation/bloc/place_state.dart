part of 'place_bloc.dart';

@freezed
class PlaceState with _$PlaceState {
  const factory PlaceState({
    @Default(PlaceStatus.initial) PlaceStatus status,
    @Default([]) List<PlaceEntity> defaultPlaces,
    @Default([]) List<PlaceEntity> places,
    @Default('') String message,
  }) = _Initial;
}

enum PlaceStatus {
  initial,
  loading,
  fetchPlacesSuccess,
  fetchPlacesError,
  searchPlacesSuccess;
}
