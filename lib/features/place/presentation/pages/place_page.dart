import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:mini_project/cores/widgets/fetch_error_widget.dart';
import 'package:mini_project/cores/widgets/search_widget.dart';
import 'package:mini_project/features/place/data/models/place_model.dart';
import 'package:mini_project/features/place/presentation/bloc/place_bloc.dart';
import 'package:mini_project/features/place/presentation/widgets/place_item_widget.dart';

class PlacePage extends StatefulWidget {
  const PlacePage({super.key});

  @override
  State<PlacePage> createState() => _PlacePageState();
}

class _PlacePageState extends State<PlacePage> {
  late final PlaceBloc _placeBloc = context.read<PlaceBloc>();

  final TextEditingController _searchController = TextEditingController();
  final FocusNode _searchFocus = FocusNode();

  Timer? _debounce;

  @override
  void dispose() {
    super.dispose();

    _searchController.dispose();
    _searchFocus.dispose();
    _debounce?.cancel();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text(
          'Place',
          style: TextStyle(fontWeight: FontWeight.bold),
        ),
        backgroundColor: Colors.green,
        centerTitle: true,
      ),
      body: SafeArea(
          child: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 24),
        child: Column(
          children: [
            SearchWidget(
                searchController: _searchController,
                searchFocus: _searchFocus,
                onChanged: (value) {
                  if (_debounce?.isActive ?? false) _debounce?.cancel();

                  _debounce = Timer(const Duration(milliseconds: 500), () {
                    _placeBloc.add(SearchPlacesEvent(searchQuery: value));
                  });
                }),
            const SizedBox(
              height: 16,
            ),
            Expanded(
                child: BlocBuilder<PlaceBloc, PlaceState>(
              bloc: _placeBloc,
              builder: (_, state) {
                if (state.status == PlaceStatus.loading) {
                  return const Center(
                    child: SizedBox.square(
                      dimension: 26,
                      child: CircularProgressIndicator.adaptive(
                          valueColor:
                              AlwaysStoppedAnimation<Color>(Colors.green)),
                    ),
                  );
                } else if (state.status == PlaceStatus.fetchPlacesError) {
                  return Center(
                    child: FetchErrorWidget(
                      errorMessage: state.message,
                      onTapReload: () =>
                          _placeBloc.add(const FetchPlacesEvent()),
                    ),
                  );
                } else if (state.status == PlaceStatus.searchPlacesSuccess &&
                    state.places.isEmpty) {
                  return const Center(
                    child: Text('No Found Places \nTry Another Place Name',
                        textAlign: TextAlign.center),
                  );
                }

                return ListView.builder(
                    itemCount: state.places.length,
                    itemBuilder: (_, index) {
                      final place = state.places[index];
                      return PlaceItemWidget(place: place);
                    });
              },
            ))
          ],
        ),
      )),
    );
  }
}
