import 'package:flutter/material.dart';
import 'package:mini_project/features/place/data/models/place_model.dart';
import 'package:mini_project/features/place/domain/entities/place_entity.dart';

class PlaceItemWidget extends StatelessWidget {
  const PlaceItemWidget({super.key, required this.place});

  final PlaceEntity place;

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 100,
      margin: const EdgeInsets.only(bottom: 10),
      padding: const EdgeInsets.symmetric(vertical: 10),
      child: ListTile(
        leading: ClipRRect(
          borderRadius: const BorderRadius.all(Radius.circular(5)),
          child: Image.network(
              place.type == PlaceType.image ? place.image! : place.media.first,
              errorBuilder: (_, __, stackTrace) {
                return Center(
                  child: Text(stackTrace.toString()),
                );
              },
              width: 100,
              fit: BoxFit.cover,
              loadingBuilder: (context, child, loadingProgress) {
                if (loadingProgress == null) {
                  return child;
                }

                return Container(
                  width: 100,
                  alignment: Alignment.center,
                  child: const SizedBox.square(
                    dimension: 20,
                    child: CircularProgressIndicator.adaptive(
                        valueColor:
                            AlwaysStoppedAnimation<Color>(Colors.green)),
                  ),
                );
              }),
        ),
        title: Text(
          place.title,
          style: const TextStyle(fontWeight: FontWeight.bold),
        ),
        subtitle: Text(place.content),
      ),
    );
  }
}
