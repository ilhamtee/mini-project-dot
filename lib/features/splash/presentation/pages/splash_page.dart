import 'package:flutter/material.dart';
import 'package:mini_project/cores/constants/preferences_constant.dart';
import 'package:mini_project/features/home/presentation/pages/home_page.dart';
import 'package:mini_project/features/login/presentation/pages/login_page.dart';
import 'package:mini_project/injector.dart';
import 'package:shared_preferences/shared_preferences.dart';

class SplashPage extends StatefulWidget {
  const SplashPage({super.key});

  @override
  State<SplashPage> createState() => _SplashPageState();
}

class _SplashPageState extends State<SplashPage> {
  @override
  void initState() {
    final bool? isLogin =
        getIt<SharedPreferences>().getBool(PreferencesConstant.isLogin);

    WidgetsBinding.instance.addPostFrameCallback((_) {
      Future.delayed(const Duration(seconds: 2), () {
        if (isLogin == true) {
          Navigator.pushReplacement(
              context, MaterialPageRoute(builder: (_) => const HomePage()));
        } else {
          Navigator.pushReplacement(
              context, MaterialPageRoute(builder: (_) => const LoginPage()));
        }
      });
    });

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: Center(
        child: Image.network(
          'https://lokernyamedan.files.wordpress.com/2022/07/logo-digdaya-olah-teknologi-lupakerja-loker-medan-terbaru-2022.png',
          width: 100,
          height: 100,
        ),
      ),
    );
  }
}
