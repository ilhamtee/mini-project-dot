import 'package:dartz/dartz.dart';
import 'package:dio/dio.dart';
import 'package:injectable/injectable.dart';
import 'package:mini_project/cores/constants/url_constant.dart';
import 'package:mini_project/cores/exception/exception.dart';
import 'package:mini_project/cores/extensions/dio_response_extension.dart';
import 'package:mini_project/cores/helpers/dio_helper.dart';
import 'package:mini_project/cores/use_case/use_case.dart';
import 'package:mini_project/features/user_profile/data/data_sources/remote/user_profile_remote_datasource.dart';
import 'package:mini_project/features/user_profile/data/models/user_profile_model.dart';

@LazySingleton(as: UserProfileRemoteDataSource)
class UserProfileRemoteDataSourceImpl implements UserProfileRemoteDataSource {
  UserProfileRemoteDataSourceImpl(this._dio);

  final Dio _dio;

  @override
  Future<Either<Failures, UserProfileModel>> getUserProfile(
      NoParams noParams) async {
    try {
      final response = await _dio.get(
        URLConstant.getUserProfile,
      );

      if (response.isOk) {
        final UserProfileModel mappedUserProfile =
            UserProfileModel.fromJson(response.data['data']);

        return Right(mappedUserProfile);
      }

      return Right(UserProfileModel.empty());
    } on DioError catch (e) {
      final message = DioHelper.generalException(e);
      return Left(ServerFailure(errorMessage: message));
    }
  }
}
