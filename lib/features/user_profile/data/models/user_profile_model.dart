import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:mini_project/features/user_profile/domain/entities/user_profile_entity.dart';

part 'user_profile_model.freezed.dart';
part 'user_profile_model.g.dart';

@freezed
class UserProfileModel with _$UserProfileModel {
  factory UserProfileModel({
    required int id,
    required String username,
    @JsonKey(name: 'fullname') required String fullName,
    required String email,
    required String phone,
    required String avatar,
  }) = _UserProfileModel;

  factory UserProfileModel.fromJson(Map<String, dynamic> json) =>
      _$UserProfileModelFromJson(json);

  static UserProfileModel empty() {
    return UserProfileModel(
        id: 0,
        username: '-',
        fullName: '-',
        email: '-',
        phone: '-',
        avatar: '-');
  }
}

extension UserProfileModelX on UserProfileModel {
  UserProfileEntity toDomain() => UserProfileEntity(
      username: username,
      fullName: fullName,
      email: email,
      phone: phone,
      avatar: avatar);
}
