import 'package:dartz/dartz.dart';
import 'package:injectable/injectable.dart';
import 'package:mini_project/cores/exception/exception.dart';
import 'package:mini_project/cores/use_case/use_case.dart';
import 'package:mini_project/features/user_profile/data/data_sources/remote/user_profile_remote_datasource.dart';
import 'package:mini_project/features/user_profile/data/models/user_profile_model.dart';
import 'package:mini_project/features/user_profile/domain/repositories/user_profile_repository.dart';

@LazySingleton(as: UserProfileRepository)
class UserProfileRepositoryImpl implements UserProfileRepository {
  UserProfileRepositoryImpl(this._remoteDataSource);

  final UserProfileRemoteDataSource _remoteDataSource;

  @override
  Future<Either<Failures, UserProfileModel>> getUserProfile(
      NoParams noParams) async {
    return await _remoteDataSource.getUserProfile(noParams);
  }
}
