import 'package:equatable/equatable.dart';

class UserProfileEntity extends Equatable {
  const UserProfileEntity(
      {required this.username,
      required this.fullName,
      required this.email,
      required this.phone,
      required this.avatar});

  final String username;
  final String fullName;
  final String email;
  final String phone;
  final String avatar;

  @override
  List<Object?> get props => [username, fullName, email, phone, avatar];
}
