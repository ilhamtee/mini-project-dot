import 'package:dartz/dartz.dart';
import 'package:mini_project/cores/exception/exception.dart';
import 'package:mini_project/cores/use_case/use_case.dart';
import 'package:mini_project/features/user_profile/data/models/user_profile_model.dart';

abstract class UserProfileRepository {
  Future<Either<Failures, UserProfileModel>> getUserProfile(
    NoParams noParams,
  );
}
