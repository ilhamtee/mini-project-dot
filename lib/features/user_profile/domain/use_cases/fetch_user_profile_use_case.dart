import 'package:dartz/dartz.dart';
import 'package:injectable/injectable.dart';
import 'package:mini_project/cores/exception/exception.dart';
import 'package:mini_project/cores/use_case/use_case.dart';
import 'package:mini_project/features/user_profile/data/models/user_profile_model.dart';
import 'package:mini_project/features/user_profile/domain/repositories/user_profile_repository.dart';

@injectable
class FetchUserProfileUseCase implements UseCase<UserProfileModel, NoParams> {
  const FetchUserProfileUseCase(this._repository);
  final UserProfileRepository _repository;

  @override
  Future<Either<Failures, UserProfileModel>> call(
    NoParams params,
  ) async {
    return await _repository.getUserProfile(params);
  }
}
