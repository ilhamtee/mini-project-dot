import 'package:bloc/bloc.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:injectable/injectable.dart';
import 'package:mini_project/cores/use_case/use_case.dart';
import 'package:mini_project/features/user_profile/data/models/user_profile_model.dart';
import 'package:mini_project/features/user_profile/domain/entities/user_profile_entity.dart';
import 'package:mini_project/features/user_profile/domain/use_cases/fetch_user_profile_use_case.dart';

part 'profile_event.dart';
part 'profile_state.dart';
part 'profile_bloc.freezed.dart';

@injectable
class ProfileBloc extends Bloc<ProfileEvent, ProfileState> {
  ProfileBloc(this._fetchUserProfileUseCase) : super(const ProfileState()) {
    on<FetchUserProfileEvent>(_onFetchUserProfile);
  }

  final FetchUserProfileUseCase _fetchUserProfileUseCase;

  Future<void> _onFetchUserProfile(
      FetchUserProfileEvent event, Emitter<ProfileState> emit) async {
    emit(state.copyWith(status: ProfileStatus.loading));

    final fetchProfileResult = await _fetchUserProfileUseCase(NoParams());

    fetchProfileResult.fold(
        (failed) => emit(state.copyWith(
            status: ProfileStatus.fetchProfileError,
            message: failed.errorMessage)),
        (userProfileModel) => emit(state.copyWith(
            status: ProfileStatus.fetchProfileSuccess,
            userProfile: userProfileModel.toDomain())));
  }
}
