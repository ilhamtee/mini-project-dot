part of 'profile_bloc.dart';

@freezed
class ProfileState with _$ProfileState {
  const factory ProfileState({
    @Default(ProfileStatus.initial) ProfileStatus status,
    UserProfileEntity? userProfile,
    @Default('') String message,
  }) = _Initial;
}

enum ProfileStatus {
  initial,
  loading,
  fetchProfileSuccess,
  fetchProfileError;
}
