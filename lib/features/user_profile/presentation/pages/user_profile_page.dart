import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:mini_project/cores/widgets/fetch_error_widget.dart';
import 'package:mini_project/features/user_profile/presentation/bloc/profile_bloc.dart';
import 'package:mini_project/features/user_profile/presentation/widgets/profile_info_widget.dart';

class UserProfilePage extends StatefulWidget {
  const UserProfilePage({super.key});

  @override
  State<UserProfilePage> createState() => _UserProfilePageState();
}

class _UserProfilePageState extends State<UserProfilePage> {
  late final ProfileBloc _profileBloc = context.read<ProfileBloc>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text(
          'User',
          style: TextStyle(fontWeight: FontWeight.bold),
        ),
        backgroundColor: Colors.green,
        centerTitle: true,
      ),
      body: BlocBuilder<ProfileBloc, ProfileState>(
        bloc: _profileBloc,
        builder: (context, state) {
          if (state.status == ProfileStatus.loading) {
            return const Center(
              child: SizedBox.square(
                dimension: 26,
                child: CircularProgressIndicator.adaptive(
                    valueColor: AlwaysStoppedAnimation<Color>(Colors.green)),
              ),
            );
          } else if (state.status == ProfileStatus.fetchProfileError) {
            return Center(
              child: FetchErrorWidget(
                errorMessage: state.message,
                onTapReload: () =>
                    _profileBloc.add(const FetchUserProfileEvent()),
              ),
            );
          }

          return SafeArea(
            child: Padding(
              padding: const EdgeInsets.symmetric(horizontal: 16, vertical: 40),
              child: Column(
                children: [
                  CircleAvatar(
                    radius: 50,
                    backgroundImage:
                        NetworkImage(state.userProfile?.avatar ?? ''),
                    backgroundColor: Colors.green.shade300,
                  ),
                  const SizedBox(
                    height: 24,
                  ),
                  ProfileInfoWidget(
                    infoLabel: 'Username',
                    infoValue: state.userProfile?.username,
                  ),
                  const SizedBox(
                    height: 10,
                  ),
                  ProfileInfoWidget(
                    infoLabel: 'Full Name',
                    infoValue: state.userProfile?.fullName,
                  ),
                  const SizedBox(
                    height: 10,
                  ),
                  ProfileInfoWidget(
                    infoLabel: 'Email',
                    infoValue: state.userProfile?.email,
                  ),
                  const SizedBox(
                    height: 10,
                  ),
                  ProfileInfoWidget(
                    infoLabel: 'Phone',
                    infoValue: state.userProfile?.phone,
                  ),
                ],
              ),
            ),
          );
        },
      ),
    );
  }
}
