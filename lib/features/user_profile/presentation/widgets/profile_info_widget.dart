import 'package:flutter/material.dart';

class ProfileInfoWidget extends StatelessWidget {
  const ProfileInfoWidget(
      {super.key, required this.infoLabel, required this.infoValue});

  final String infoLabel;
  final String? infoValue;

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        SizedBox(
            width: 80,
            child: Text(
              infoLabel,
              style: const TextStyle(fontSize: 14, fontWeight: FontWeight.w500),
            )),
        Expanded(
            child: Text(
          ': $infoValue',
          style: const TextStyle(fontSize: 14, fontWeight: FontWeight.w500),
        ))
      ],
    );
  }
}
