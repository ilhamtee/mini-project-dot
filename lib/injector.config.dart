// GENERATED CODE - DO NOT MODIFY BY HAND

// **************************************************************************
// InjectableConfigGenerator
// **************************************************************************

// ignore_for_file: unnecessary_lambdas
// ignore_for_file: lines_longer_than_80_chars
// coverage:ignore-file

// ignore_for_file: no_leading_underscores_for_library_prefixes
import 'package:dio/dio.dart' as _i3;
import 'package:get_it/get_it.dart' as _i1;
import 'package:injectable/injectable.dart' as _i2;
import 'package:mini_project/cores/helpers/dio_helper.dart' as _i4;
import 'package:mini_project/cores/modules/network_module.dart' as _i27;
import 'package:mini_project/cores/modules/storage_module.dart' as _i26;
import 'package:mini_project/features/gallery/data/data_sources/remote/gallery_remote_datasource.dart'
    as _i5;
import 'package:mini_project/features/gallery/data/data_sources/remote/gallery_remote_datasource_impl.dart'
    as _i6;
import 'package:mini_project/features/gallery/data/repositories/gallery_repository_impl.dart'
    as _i8;
import 'package:mini_project/features/gallery/domain/repositories/gallery_repository.dart'
    as _i7;
import 'package:mini_project/features/gallery/domain/use_cases/fetch_galleries_use_case.dart'
    as _i20;
import 'package:mini_project/features/gallery/presentation/bloc/gallery_bloc.dart'
    as _i23;
import 'package:mini_project/features/home/presentation/bloc/home_cubit.dart'
    as _i9;
import 'package:mini_project/features/login/presentation/bloc/login_bloc.dart'
    as _i10;
import 'package:mini_project/features/place/data/data_sources/remote/place_remote_datasource.dart'
    as _i11;
import 'package:mini_project/features/place/data/data_sources/remote/place_remote_datasource_impl.dart'
    as _i12;
import 'package:mini_project/features/place/data/repositories/place_repository_impl.dart'
    as _i14;
import 'package:mini_project/features/place/domain/repositories/place_repository.dart'
    as _i13;
import 'package:mini_project/features/place/domain/use_cases/fetch_places_use_case.dart'
    as _i21;
import 'package:mini_project/features/place/presentation/bloc/place_bloc.dart'
    as _i24;
import 'package:mini_project/features/user_profile/data/data_sources/remote/user_profile_remote_datasource.dart'
    as _i16;
import 'package:mini_project/features/user_profile/data/data_sources/remote/user_profile_remote_datasource_impl.dart'
    as _i17;
import 'package:mini_project/features/user_profile/data/repositories/user_profile_repository_impl.dart'
    as _i19;
import 'package:mini_project/features/user_profile/domain/repositories/user_profile_repository.dart'
    as _i18;
import 'package:mini_project/features/user_profile/domain/use_cases/fetch_user_profile_use_case.dart'
    as _i22;
import 'package:mini_project/features/user_profile/presentation/bloc/profile_bloc.dart'
    as _i25;
import 'package:shared_preferences/shared_preferences.dart' as _i15;

extension GetItInjectableX on _i1.GetIt {
  // initializes the registration of main-scope dependencies inside of GetIt
  Future<_i1.GetIt> init({
    String? environment,
    _i2.EnvironmentFilter? environmentFilter,
  }) async {
    final gh = _i2.GetItHelper(
      this,
      environment,
      environmentFilter,
    );
    final networkModule = _$NetworkModule();
    final storageModule = _$StorageModule();
    gh.lazySingleton<_i3.Dio>(() => networkModule.dio);
    gh.factory<_i4.DioHelper>(() => _i4.DioHelper());
    gh.lazySingleton<_i5.GalleryRemoteDataSource>(
        () => _i6.GalleryRemoteDataSourceImpl(gh<_i3.Dio>()));
    gh.lazySingleton<_i7.GalleryRepository>(
        () => _i8.GalleryRepositoryImpl(gh<_i5.GalleryRemoteDataSource>()));
    gh.factory<_i9.HomeCubit>(() => _i9.HomeCubit());
    gh.factory<_i10.LoginBloc>(() => _i10.LoginBloc());
    gh.lazySingleton<_i11.PlaceRemoteDataSource>(
        () => _i12.PlaceRemoteDataSourceImpl(gh<_i3.Dio>()));
    gh.lazySingleton<_i13.PlaceRepository>(
        () => _i14.PlaceRepositoryImpl(gh<_i11.PlaceRemoteDataSource>()));
    await gh.lazySingletonAsync<_i15.SharedPreferences>(
      () => storageModule.sharedPreference,
      preResolve: true,
    );
    gh.lazySingleton<_i16.UserProfileRemoteDataSource>(
        () => _i17.UserProfileRemoteDataSourceImpl(gh<_i3.Dio>()));
    gh.lazySingleton<_i18.UserProfileRepository>(() =>
        _i19.UserProfileRepositoryImpl(gh<_i16.UserProfileRemoteDataSource>()));
    gh.factory<_i20.FetchGalleriesUseCase>(
        () => _i20.FetchGalleriesUseCase(gh<_i7.GalleryRepository>()));
    gh.factory<_i21.FetchPlacesUseCase>(
        () => _i21.FetchPlacesUseCase(gh<_i13.PlaceRepository>()));
    gh.factory<_i22.FetchUserProfileUseCase>(
        () => _i22.FetchUserProfileUseCase(gh<_i18.UserProfileRepository>()));
    gh.factory<_i23.GalleryBloc>(
        () => _i23.GalleryBloc(gh<_i20.FetchGalleriesUseCase>()));
    gh.factory<_i24.PlaceBloc>(
        () => _i24.PlaceBloc(gh<_i21.FetchPlacesUseCase>()));
    gh.factory<_i25.ProfileBloc>(
        () => _i25.ProfileBloc(gh<_i22.FetchUserProfileUseCase>()));
    return this;
  }
}

class _$StorageModule extends _i26.StorageModule {}

class _$NetworkModule extends _i27.NetworkModule {}
