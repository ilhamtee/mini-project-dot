import 'package:flutter/material.dart';
import 'package:mini_project/cores/constants/url_constant.dart';
import 'package:mini_project/cores/helpers/dio_helper.dart';
import 'package:mini_project/features/gallery/presentation/bloc/gallery_bloc.dart';
import 'package:mini_project/features/home/presentation/bloc/home_cubit.dart';
import 'package:mini_project/features/login/presentation/bloc/login_bloc.dart';
import 'package:mini_project/features/place/presentation/bloc/place_bloc.dart';
import 'package:mini_project/features/splash/presentation/pages/splash_page.dart';
import 'package:mini_project/features/user_profile/presentation/bloc/profile_bloc.dart';
import 'package:mini_project/injector.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();

  // Initialize Dependency Injection
  await configureDependencies();

  DioHelper.initialDio(URLConstant.baseURL);

  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MultiBlocProvider(
      providers: [
        BlocProvider(create: (context) => getIt<LoginBloc>()),
        BlocProvider(create: (context) => getIt<HomeCubit>()),
        BlocProvider(create: (context) => getIt<PlaceBloc>()),
        BlocProvider(create: (context) => getIt<GalleryBloc>()),
        BlocProvider(create: (context) => getIt<ProfileBloc>()),
      ],
      child: MaterialApp(
        title: 'Mini Project',
        theme: ThemeData(primaryColor: Colors.green),
        debugShowCheckedModeBanner: false,
        home: const SplashPage(),
      ),
    );
  }
}
